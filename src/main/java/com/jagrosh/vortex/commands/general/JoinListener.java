package com.jagrosh.vortex.commands.general;

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class JoinListener extends ListenerAdapter {

	@Override
	public void onGuildMemberJoin(GuildMemberJoinEvent event) {
		Member member = event.getMember();
		TextChannel channel;
				
				
		if((channel = event.getGuild().getDefaultChannel()) != null) {
			channel.sendMessage("Hallo und Herzlich Willkommen " + member.getAsMention() + ", auf " + event.getGuild().getName() + ", bitte lies dir die Regeln durch! :smirk:").queue();
		}
	}
	
	@Override
	public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
		Member member = event.getMember();
		TextChannel channel;
				
				
		if((channel = event.getGuild().getSystemChannel()) != null) {
			channel.sendMessage("Bis zum nÃ¤chsten mal " + member.getAsMention()).queue();
		}
	}
	
}
